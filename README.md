# Imperial Library of Trantor

The Imperial Library of Trantor (also known as Galactic Library) is a 
repository management system of ebooks on ePub format.

You can check out the main development branch from GitLab at:

https://gitlab.com/trantor/

## Dependences

In order to run Trantor, you need to install the following packages:

* Go language
* postgresql (>= 9.6)

## Installation

### Create postgres DB

We can create the DB (using the postgres user) and activate `pg_trgm` extension
for it:
```
$ createdb trantor
$ echo "CREATE EXTENSION pg_trgm;"|psql trantor
```

### For admins ("for developers" below)

Now you can install Trantor itself:
```
# go get gitlab.com/trantor/trantor
```

You can run it (using `/var/lib/trantor` for storage):
```
# $GOPATH/bin/trantor -assets $GOPATH/src/pkg/gitlab.com/trantor/trantor/ -store /var/lib/trantor
```

Go to your browser to: http://localhost:8080

### For developers

Now you can compile Trantor:
```
$ go get .
$ go build
```

Now you can run it:
```
$ ./trantor
```

Go to your browser to: http://localhost:8080

## Bugs

Please, report bugs in the gitlab issue tracker:  
https://gitlab.com/trantor/trantor/issues

## Rights

All the matterial of this project is under WTFPL as described on the LICENSE 
file with the exception of:
* css/bootstrap.min.css css/bootstra-responsive.min.css js/bootstrap.min.js 
  img/glyphicons-halflings-white.png img/glyphicons-halflings.png  
  From the bootstrap framework: http://twitter.github.com/bootstrap/
* js/jquery.js  
  From jquery library: http://jquery.com/
* img/bright_squares.png  
  From subtlepatterns: http://subtlepatterns.com/bright-squares/
* css/FredokaOne.ttf css/PTSerif.ttf  
  From Google Web Fonts: http://www.google.com/webfonts
* js/bootstrap-tokenfield.min.js dist/css/bootstrap-tokenfield.min.css  
  From Bootstrap Tokenfield: https://github.com/sliptree/bootstrap-tokenfield
