package parser

import "testing"

func TestISBN(t *testing.T) {
	isbn_test := [][]string{
		[]string{"", ""},
		[]string{"978074341", ""},
		[]string{"9780743412395", ""},
		[]string{"9780743412391", "9780743412391"},
		[]string{"0-688-12189-6", "9780688121891"},
		[]string{"033026155X", "9780330261555"},
		[]string{"033026155x", "9780330261555"},
		[]string{"0307756432", "9780307756435"},
		[]string{"urn:isbn:978-3-8387-0337-4:", "9783838703374"},
		[]string{"EPUB9788865971468-113465", "9788865971468"},
	}

	for _, isbn := range isbn_test {
		src := isbn[0]
		dst := isbn[1]
		if res := ISBN(src); res != dst {
			t.Error("ISBN parse failed: ", src, " => ", res, " (expected ", dst, ")")
		}
	}
}
