package database

import "testing"

const (
	name, pass = "user", "mypass"
)

func TestUserEmpty(t *testing.T) {
	db, dbclose := testDbInit(t)
	defer dbclose()

	if db.ValidPassword("", "") {
		t.Errorf("ValidPassword() with an empty password return true")
	}
}

func TestAddUser(t *testing.T) {
	db, dbclose := testDbInit(t)
	defer dbclose()

	testAddUser(t, db)
	if !db.ValidPassword(name, pass) {
		t.Errorf("ValidPassword() return false for a valid user")
	}
}

func TestEmptyUsername(t *testing.T) {
	db, dbclose := testDbInit(t)
	defer dbclose()

	testAddUser(t, db)
	if db.ValidPassword("", pass) {
		t.Errorf("ValidPassword() return true for an invalid user")
	}
}

func testAddUser(t *testing.T, db DB) {
	err := db.AddUser(name, pass)
	if err != nil {
		t.Errorf("db.Adduser(%v, %v) return an error: %v", name, pass, err)
	}
}
